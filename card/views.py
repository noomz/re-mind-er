from uuid import uuid1
from calendar import month_name
from collections import defaultdict

from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django import VERSION

from mezzanine.blog.models import BlogPost, BlogCategory
from mezzanine.blog.feeds import PostsRSS, PostsAtom
from mezzanine.conf import settings
from mezzanine.generic.models import AssignedKeyword, Keyword
from mezzanine.utils.views import render, paginate

from card.models import Card


def randstr():
    return str(uuid1())[0: 5].replace('-', '')


def card_view(request, slug, template="card/card.html"):
    article_posts = Card.objects.published(for_user=request.user)
    article_post = get_object_or_404(article_posts, slug=slug)
    context = {
        "card": article_post,
        "editable_obj": article_post
    }
    templates = [u"card/card_%s.html" % unicode(slug), template]
    return render(request, templates, context)


def card_list(request, template="card/list.html"):
    settings.use_editable()
    templates = []
    article_posts = Card.objects.published()

    context = {
        "cards": article_posts
    }
    templates.append(template)
    return render(request, templates, context)


def card_create(request, template="card/create.html"):

    templates = []

    if request.method == 'POST':
        card_type = request.POST.get('type')
        title = '%s %s' % (card_type, randstr())
        content = request.POST.get('content')
        author = request.user

        if card_type == 'p':
            is_saved = True
        else:
            is_saved = False

        Card.objects.create(
            title = title,
            content = content,
            user_id = author.id,
            is_saved = is_saved,
        )
        templates.append(template)
    else:
        templates.append(template)

    return render(request, templates, {})