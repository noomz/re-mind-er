from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

from mezzanine.core.views import direct_to_template


admin.autodiscover()

# Add the urlpatterns for any custom Django applications here.
# You can also change the ``home`` view to add your own functionality
# to the project's homepage.

urlpatterns = patterns("card.views",
    url("^list/$", "card_list", name="card_list"),

    url("^create/$", "card_create", name="card_create"),

    url("^(?P<slug>.*)/view/$", "card_view", name="card_view"),

)