from django.utils.translation import ugettext_lazy as _

from django.core.urlresolvers import resolve, reverse
from django.db import models
from mezzanine.core.models import Orderable, Displayable, Ownable, RichText
from mezzanine.pages.models import Page
from django.contrib import admin
from mezzanine.core.fields import FileField
from mezzanine.pages.admin import PageAdmin
from mezzanine.utils.models import AdminThumbMixin

from django.contrib.auth.models import User


class Card(Displayable, Ownable, RichText, AdminThumbMixin):
    '''Card'''
    is_saved = models.BooleanField(_("Save"),
            help_text=_("If checked, it'll show cards."), default=False)

    class Meta:
        verbose_name = _("Card")
        verbose_name_plural = _("Card")
        ordering = ("-publish_date",)

    @models.permalink
    def get_absolute_url(self):
        kwargs = {"slug": self.slug}
        return ("card_view", (), kwargs)

admin.site.register(Card)