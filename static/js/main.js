jQuery(document).ready(function ($) {

	var state_0 = function () {
		$('#homepage-form-container').slideUp();
		$('#select-pd-container').slideDown();
		$('#homepage-form actions').fadeIn();
	};

	var state_1 = function () {
		$('#homepage-form-container').slideDown();
		$('#select-pd-container').slideUp();
		$('#homepage-form actions').fadeOut();
	};

	var state_p = function () {
		$('#select-p-confirm').show();
		$('#select-d-confirm').hide();
	};

	var state_d = function () {
		$('#select-d-confirm').show();
		$('#select-p-confirm').hide();
	};

	$('#select-p, #select-d').click(function (e) {
		state_1();

		if ($(this).attr('id') === 'select-p') {
			state_p();
		}
		else {
			state_d();
		}
	});

	var submit_data = function (emo_type) {
		if (emo_type === 'p') {
			$('input[name="type"]').val('p');
		}
		else {
			$('input[name="type"]').val('d');
		}

		$('#homepage-form form').submit();
	};

	$("#select-p-confirm").click(function (e) {
		submit_data('p');
	});

	$("#select-d-confirm").click(function (e) {
		$('#homepage-form textarea').addClass('burning-text');

		setTimeout(function () {
			submit_data('d');
		}, 2000);
		
	});

	$('button.cancel').click(function (e) {
		e.preventDefault();
		state_0();
	});

	// var gauge = new Gauge($('.guage')).setOptions({
	// 	lines: 12, // The number of lines to draw
	// 	angle: 0.15, // The length of each line
	// 	lineWidth: 0.44, // The line thickness
	// 	pointer: {
	// 	length: 0.9, // The radius of the inner circle
	// 	strokeWidth: 0.035, // The rotation offset
	// 	color: '#000000' // Fill color
	// 	},
	// 	colorStart: '#6FADCF',   // Colors
	// 	colorStop: '#8FC0DA',    // just experiment with them
	// 	strokeColor: '#E0E0E0',   // to see which ones work best for you
	// 	generateGradient: true
	// });
	// gauge.set(1675);
});

// Angular
